"""
Use-case of my vk api
"""
import vkapi

def console_interface_routine():
    """ Command-line user interface """
    c_key = input('Get vk? (y/n=exit/def=y): ')
    if c_key == "" or c_key == 'y':
        vk = vkapi.get_vk()
    else:
        print('Exit initated')
        return

    while True:
        try:
            c_key = input('Command: ')
            c_key = c_key.lower()
            if c_key == 'exit':
                return
            if c_key == 'login':
                vk.login()
            if c_key == 'status':
                print(vk.get_status())
            if c_key == 'set status':
                new_status = input("    New status: ")
                print(vk.set_status(new_status))
            if c_key == 'get':
                count = int(input("    How many messages: "))
                print(vk.get_last_messages(0, count))
            if c_key == 'upload photo':
                print(vk.messages.send_photo('',
                                       ""))

        except vkapi.NotLoggedException:
            print("You need to be logged in for this.")




console_interface_routine()
