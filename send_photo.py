import vkapi
from vk_raw_api import NotLoggedException
import sys

def send_photo(uid, path):
    """ send phot to a user """
    vk = vkapi.get_vk()
    token_file = 'token'
    try:
        session_token_file = open(vkapi.get_real_path(token_file))
        token = session_token_file.read().splitlines()[0]
        vk.token_update(token)
        session_token_file.close()
    except FileNotFoundError:
        print("File '%s' was not found." % token_file)
        vk.login()

    try:
        vk.messages.send_photo(uid, path)
    except NotLoggedException:
        vk.login()

if len(sys.argv) != 3:
    print("Wrong number of parameters")
    sys.exit(0)
send_photo(sys.argv[1], sys.argv[2])
