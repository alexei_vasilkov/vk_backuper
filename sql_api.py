"""
Layer between python and MySQL db for vk
"""
import mysql.connector


db = mysql.connector.connect(user='simple_cms', password='secret',
                             host='127.0.0.1',
                             database='simple_cms_development')
cursor = db.cursor()
query= "SELECT first_name FROM admin_users"
cursor.execute(query)
for first_name in cursor:
    print(first_name)
cursor.close()
db.close()
