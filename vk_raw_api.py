"""
Raw vk api requests
"""
import json
#import getpass
from urllib.request import urlopen
from urllib.parse import urlencode as urlencode
import time

class NotLoggedException(Exception):
    """ Not logged in exception """
    pass

def captcha(data):
    """Ask user to solve captcha
    TODO
    not tested
    """
    print("VK thinks you're a bot - and you are ;)")
    print("They want you to solve CAPTCHA. Please open this URL," +
          " and type here a captcha solution:")
    print("\n\t{}\n".format(data[u'error'][u'captcha_img']))
    solution = input("Solution = ").strip()
    return data[u'error'][u'captcha_sid'], solution

def call_api(method, params, token):
    """ forms url, calls api method and returns json """
    if token == "1":
        raise NotLoggedException("You need to be logged in for this")
    params.append(("access_token", token))
    url = "https://api.vk.com/method/%s?%s" % (method, urlencode(params))
    response = json.loads(urlopen(url).read().decode('utf-8'))
    #Error checking
    if u'error' in response.keys():
        if response[u'error'][u'error_code'] == 6:  # too many requests
            print("Too many requests per second, sleeping..")
            time.sleep(0.4)
        elif response[u'error'][u'error_code'] == 14:  # captcha needed :\
            sid, key = captcha(response)
            params.extend([(u"captcha_sid", sid), (u"captcha_key", key)])
        else:
            msg = "API call resulted in error ({}): {}". \
                format(response[u'error'][u'error_code'],
                       response[u'error'][u'error_msg'])
            raise NotLoggedException(msg)

    response = response["response"]
    return response

def get_user_status(token, user_id):
    """Usage example: status = get_user_status(user_id, token) """
    apicall = "status.get"
    response = call_api(apicall, [("uid", user_id)], token)
    return response['text']

def set_user_status(token, new_status):
    """Usage example: True/False = set_user_status(token, new_status) """
    apicall = "status.set"
    response = call_api(apicall, [("text", new_status)], token)
    return response == 1
