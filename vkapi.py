"""
vk api calls here
use it inside some object
I'm too lazy to continue write this docstring further
"""
import vk_auth
from vk_raw_api import *
import requests
import ast
import os

def get_vk():
    """ Enter user credentials and create VK obkectk.
    File format:
        email
        password
        app_id
        perm1,perm2,permN
    """
    user_cred_file_name = 'user_credentials'
    try:
        user_cred_file = open(get_real_path(user_cred_file_name))
        user_creds = user_cred_file.read().splitlines()
        user_cred_file.close()
    except FileNotFoundError:
        print("File '%s' was not found." % user_cred_file_name)
        return
    if len(user_creds) != 4:
        print("File is not in appropriate format.")
        return
    email = user_creds[0]
    password = user_creds[1]
    app_id = user_creds[2]
    permissions = user_creds[3].split(",\n")
    return VK(email, password, app_id, permissions)


class UserMessages:
    """ User messages processor """
    def __init__(self, photos, token):
        self.token = token
        self.photos = photos
    def get(self, out, offset, count):
        """ gets list of dictionary message objects """
        apicall = "messages.get"
        msg_dict_list = call_api(apicall, [("out", out), ("offset", offset),
                                           ("count", count)], self.token)[1:]
        return msg_dict_list

    def send_photo(self, to_uid, local_path):
        """ Sends message to user with to_uid id """
        apicall = "messages.send"
        #owner id and photo id
        owd, pid = self.photos.upload_photo_for_messages(local_path)
        call_api(apicall, [('user_id', to_uid), ('attachment', 'photo%s_%s'
                                                 %(owd, pid))], self.token)

class UserPhotos:
    """ VK photos processor """
    def __init__(self, token):
        self.token = token
    def upload_photo_for_messages(self, local_path):
        """ Recieves url to upload, uploads and saves photo in messages.
        But not sends it.
        """
        apicall = "photos.getMessagesUploadServer"
        upload_url = call_api(apicall, [], self.token)['upload_url']
        files = {'file': open(local_path, 'rb')}
        response = requests.post(upload_url, files=files)
        #http://stackoverflow.com/questions/988228/converting-a-string-to-dictionary
        response = ast.literal_eval(response.text)
        photo = response['photo']
        server = response['server']
        hash_1 = response['hash']
        apicall = "photos.saveMessagesPhoto"
        response = call_api(apicall, [("photo", photo), ('server', server),
                                      ('hash', hash_1)], self.token)[0]
        return response['owner_id'], response['pid']
def get_real_path(filename):
    """ returns absolute path of file name inside this script folder """
    __location__ = os.path.realpath(\
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
    return os.path.join(__location__, filename)


class VK:
    """ Wrapping around vkapi module """
    def __init__(self, user_email, user_password, app_id, permissions=None):
        self.email = user_email
        self.password = user_password
        self.app_id = app_id
        self.permissions = permissions
        if permissions is None:
            self.permissions = ['status']
        self.token = ""
        self.user_id = ""
        self.photos = UserPhotos("")
        self.messages = UserMessages(self.photos, "")

    def login(self):
        """Logs in to vk, asks for email/password"""
        #Enter password every time
        #email = input("Email: ")
        #password = getpass.getpass()
        #id of standallone app
        #Gets token that you'll need everywhere on each request

        self.token, self.user_id = vk_auth.auth(self.email, self.password,
                                                self.app_id, self.permissions)
        self.token_update(self.token)
        f = open(get_real_path('token'), 'w')
        f.write(self.token)
        f.close()

    def token_update(self, token):
        """ set token """
        self.token = token
        self.messages.token = self.token
        self.photos.token = self.token


    def get_status(self):
        """ Gets vk status of the logged in user """
        return get_user_status(self.token, self.user_id)
    def set_status(self, new_status):
        """ Sets vk status of the logged in user """
        return set_user_status(self.token, new_status)

    def get_last_messages(self, out, count):
        """ get last count messages """
        return self.get_messages(out, 0, count)

    def get_messages(self, out, offset, count):
        """ offset: =1 then messages will be from the position 1
        if start was 0
        """
        msg_dict_list = self.messages.get(out, offset, count)
        return msg_dict_list
